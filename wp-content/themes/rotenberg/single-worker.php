<?php

the_post();
get_header();
$fields = get_fields();
$tabs = [
		[
				'title' => lang_text(['he' => 'השכלה', 'en' => 'Education'], 'he'),
				'content' => $fields['worker_education'],
		],
		[
				'title' => lang_text(['he' => 'בין לקוחותיה', 'en' => 'Clients'], 'he'),
				'content' => $fields['worker_clients'],
		],
		[
				'title' => lang_text(['he' => 'לקוחות ממליצים', 'en' => 'Reviews'], 'he'),
				'content' => $fields['worker_reviews'],
		],
		[
				'title' => lang_text(['he' => 'הישגים', 'en' => 'Accomplishments'], 'he'),
				'content' => $fields['worker_item_last'],
		]
	];
?>
<article class="page-body worker-page-body">
	<?php if ( function_exists('yoast_breadcrumb')) : ?>
		<div class="container-fluid pt-3">
			<div class="row justify-content-center">
				<div class="col-xl-11 col-12">
					<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' ); ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<div class="container pt-3">
		<div class="row justify-content-between align-items-start mb-5">
			<div class="col-lg-6 col-12 d-flex flex-column align-items-start">
				<div class="base-output bigger-output mb-3">
					<?php the_content(); ?>
				</div>
			</div>
			<?php if (has_post_thumbnail()) : ?>
				<div class="col-xl-4 col-lg-5 worker-page-col-wrap">
					<div class="worker-img-wrap">
						<div class="about-img-wrap">
							<img src="<?= postThumb(); ?>" alt="worker-img" class="w-100">
						</div>
					</div>
					<h3 class="base-mid-title text-center">
						<?= $fields['worker_name']; ?>
					</h3>
					<h3 class="worker-position-title">
						<?= $fields['worker_position']; ?>
					</h3>
				</div>
			<?php endif; ?>
		</div>
	</div>
	<ul class="nav nav-tabs" id="myTab" role="tablist">
		<li class="nav-item">
			<a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">
				<?= lang_text(['he' => 'תחומי התמחות', 'en' => 'Areas of expertise'], 'he'); ?>
			</a>
		</li>
		<?php foreach ($tabs as $x => $tab) : if ($tab['content']) : ?>
			<li class="nav-item">
				<a class="nav-link" id="<?= $x; ?>-tab" data-toggle="tab" href="#content-<?= $x; ?>" role="tab" aria-controls="content-<?= $x; ?>" aria-selected="false">
					<?= (isset($tab['content']['item_title']) && $tab['content']['item_title']) ? $tab['content']['item_title'] : $tab['title']; ?>
				</a>
			</li>
		<?php endif; endforeach; ?>
	</ul>
	<div class="tab-content mt-0" id="myTabContent">
		<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
			<?php if ($fields['areas_item']) {
				get_template_part('views/partials/content', 'areas',
						[
								'item' => $fields['areas_item'],
						]);
			} ?>
		</div>
		<?php foreach ($tabs as $x => $tab) : if ($tab['content']) : ?>
			<div class="tab-pane fade" id="content-<?= $x; ?>" role="tabpanel" aria-labelledby="<?= $x; ?>-tab">
				<div class="areas-block">
					<div class="container">
						<div class="row">
							<div class="col-12">
								<div class="base-output">
									<?= $tab['content']['item_text']; ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php endif; endforeach; ?>
	</div>
</article>
<?php
if ($fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
		[
			'title' => $fields['faq_title'],
			'faq' => $fields['faq_item'],
		]);
endif;
get_footer(); ?>
