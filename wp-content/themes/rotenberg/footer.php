<?php

$tel = opt('tel');
$mail = opt('mail');
$facebook = opt('facebook');
$address = opt('address');
$current_id = get_the_ID();
$contact_id = getPageByTemplate('views/contact.php');
?>

<footer>
	<a id="go-top">
		<?= svg_simple(ICONS.'top-top.svg'); ?>
	</a>
	<div class="footer-main">
		<?php if ($contact_id !== $current_id) : ?>
			<div class="blue-form">
				<?php get_template_part('views/partials/repeat', 'form', [
						'title' => opt('foo_form_title'),
						'id' => '14',
				]); ?>
			</div>
		<?php endif; ?>
		<div class="container footer-container-menu">
			<div class="row justify-content-between align-items-start">
				<div class="col-lg col-md-6 col-12 foo-menu">
					<h3 class="foo-title"><?= lang_text(['he' => 'מפת האתר', 'en' => 'Site map'], 'he'); ?></h3>
					<div class="menu-border-top">
						<?php getMenu('footer-menu', '1'); ?>
					</div>
				</div>
				<div class="col-xl-5 col-lg col-md-6 col-12 foo-menu">
					<?php if ($foo_l_title = opt('foo_menu_title')) : ?>
						<h3 class="foo-title">
							<?php echo $foo_l_title ? $foo_l_title : 'קישורים'; ?>
						</h3>
					<?php endif; ?>
					<div class="menu-border-top">
						<?php getMenu('footer-links-menu', '1', 'hop-hey two-columns'); ?>
					</div>
				</div>
				<div class="col-lg col-md-6 foo-menu contacts-footer-menu">
					<h3 class="foo-title"><?= lang_text(['he' => 'צרו קשר', 'en' => 'Contact us'], 'he'); ?></h3>
					<div class="menu-border-top">
						<ul class="contact-list d-flex flex-column">
							<?php if ($tel) : ?>
								<li>
									<a href="tel:<?= $tel; ?>" class="contact-info-footer">
										<?= lang_text(['he' => 'נייד:', 'en' => 'Phone: '], 'he').$tel; ?>
									</a>
								</li>
							<?php endif; ?>
							<?php if ($address) : ?>
								<li>
									<a href="https://waze.com/ul?q=<?= $address; ?>"
									   class="contact-info-footer" target="_blank">
										<?= lang_text(['he' => 'כתובת ראשית:', 'en' => 'Main address: '], 'he').$address; ?>
									</a>
								</li>
							<?php endif; ?>
							<?php if ($mail) : ?>
								<li>
									<a href="mailto:<?= $mail; ?>" class="contact-info-footer">
										<?= lang_text(['he' => 'מייל לפניות:', 'en' => 'Email: '], 'he').$mail; ?>
									</a>
								</li>
							<?php endif; ?>
						</ul>
					</div>
				</div>
				<?php if ($facebook) : ?>
					<div class="col-lg col-md-6 facebook-widget foo-menu d-flex flex-column">
						<div class="facebook-title">
							<h3 class="foo-title"><?= lang_text(['he' => 'עשו לנו לייק', 'en' => 'Like us'], 'he'); ?></h3>
							<img src="<?= ICONS ?>facebook-like.png">
						</div>
						<div class="menu-border-top">
							<iframe src="https://www.facebook.com/plugins/page.php?href=<?= $facebook; ?>&tabs=timeline&width=300px&height=200px&small_header=true&adapt_container_width=true&hide_cover=false&show_facepile=false&appId"
									width="250px" height="150px" style="border:none;overflow:hidden" scrolling="no"
									frameborder="0" allowTransparency="true" allow="encrypted-media">
							</iframe>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<div id="leos">
		<a href="http://www.leos.co.il/" title="לאוס מדיה ואינטראקטיב">
			<img src="<?= IMG . 'leos_logo.png' ?>"
				 alt="" title="קידום אתרים עם לאוס מדיה ואינטראקטיב | חברה לקידום אתרים ובניית אתרים" />
			<span></span>
		</a>
	</div>
</footer>
<?php wp_footer(); ?>

<?php

if(ENV === 'dev'):
	require_once THEMEPATH . "/inc/debug.php"
	?>
	<script>

		function _fetchHeader($_el){
			let res = {
				'count' : 0,
				'content' : ''
			} ;
			$($_el).each(function () {
				res.count++;
				res.content += ' [' + $(this).text() + '] ';
			});
			return 'Count: ' + res.count + '. Text: ' + res.content;
		}

		function _fetchMeta($_meta){
			return $('meta[name='+$_meta+']').attr("content");
		}




		phpdebugbar.addDataSet({
			"SEO Local": {
				'H1' : _fetchHeader('h1'),
				'H2' : _fetchHeader('h2'),
				'H3' : _fetchHeader('h3'),
				'Meta Title' : _fetchMeta('title'),
				'Meta Description' : _fetchMeta('description'),
				'Meta Keywords' : _fetchMeta('keywords'),
			}
		});
	</script>

<?php endif; ?>

</body>
</html>

