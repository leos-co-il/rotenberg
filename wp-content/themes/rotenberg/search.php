<?php
get_header();
$fields = get_fields();
?>
<div class="post-output-block mb-5">
	<div class="container pt-4">
		<?php
		$s = get_search_query();
		$args_1 = array(
			'post_type' => 'post',
			's' => $s
		);
		$the_query_1 = new WP_Query( $args_1 );
		if ( $the_query_1->have_posts() ) { ?>
		<h4 class="base-title text-center my-3">
			<?= lang_text(['he' => 'תוצאות חיפוש עבור:', 'en' => 'Search results for:'], 'en');?>
			<?= get_query_var('s') ?></h4>
		<div class="row justify-content-center align-items-stretch">
			<?php while ( $the_query_1->have_posts() ) { $the_query_1->the_post();
				$link = get_the_permalink(); ?>
				<div class="col-12 col-post">
					<div class="post-card">
						<a class="post-img"<?php if (has_post_thumbnail()) : ?>
							style="background-image: url('<?= postThumb(); ?>')" <?php endif; ?> href="<?= $link; ?>">
						</a>
						<div class="post-card-content">
							<a class="base-item-title" href="<?= $link; ?>"><?php the_title() ?></a>
							<p class="base-text">
								<?= text_preview(get_the_content(), 50); ?>
							</p>
							<a href="<?= $link; ?>" class="base-link mt-3">
								<?= lang_text(['he' => 'המשך קריאה', 'en' => 'Continue reading'], 'he'); ?>
							</a>
						</div>
					</div>
				</div>
			<?php }
			} else{ ?>
				<div class="col-12 pt-5">
					<h4 class="block-title">
						<?= lang_text(['he' => 'שום דבר לא נמצא', 'en' => 'Nothing was found'], 'he'); ?>
					</h4>
				</div>
				<div class="alert alert-info text-center mt-5">
					<p><?= lang_text(['he' => 'מצטערים, אך שום דבר לא תאם את קריטריוני החיפוש שלך. אנא נסה שוב עם מילות מפתח שונות.', 'en' => '
Sorry, nothing matched your search criteria. Please, try again with different keywords.'], 'he') ?></p>
				</div>
			<?php } ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>
