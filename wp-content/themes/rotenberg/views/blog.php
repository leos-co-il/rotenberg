<?php
/*
Template Name: מאמרים
*/

get_header();
$fields = get_fields();
$posts = new WP_Query([
	'posts_per_page' => 3,
	'post_type' => 'post',
	'suppress_filters' => false,
]);
$published_posts = count(get_posts([
	'numberposts' => -1,
	'post_type' => 'post',
	'suppress_filters' => false
]));
?>

<article class="article-page-body page-body">
	<?php if ( function_exists('yoast_breadcrumb')) : ?>
		<div class="container-fluid pt-3">
			<div class="row justify-content-center">
				<div class="col-xl-11 col-12">
					<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' ); ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-lg-11 col-12">
				<h1 class="base-title text-center mb-3"><?php the_title(); ?></h1>
				<?php if ($fields['blog_links']) {
					get_template_part('views/partials/repeat', 'links', [
							'links' => $fields['blog_links'],
					]);
				} ?>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-lg-11 col-12">
				<div class="row justify-content-center align-items-start">
					<div class="col-xl-3 col-12 col-sidebar">
						<?php if ($fields['blog_side_item']) {
							get_template_part('views/partials/card', 'side', [
									'item' => $fields['blog_side_item'],
							]);
						}
						get_template_part('views/partials/repeat', 'form_side'); ?>
					</div>
					<div class="col-xl-9 col-12 col-content-side">
						<div class="base-output bigger-output mb-5">
							<?php the_content(); ?>
						</div>
						<?php if ($posts->have_posts()) : ?>
							<div class="row justify-content-center align-items-stretch put-here-posts">
								<?php foreach ($posts->posts as $i => $post) {
									get_template_part('views/partials/card', 'post', [
											'post' => $post,
									]);
								}  ?>
							</div>
						<?php endif;
						if ($published_posts && $published_posts > 3) : ?>
							<div class="row justify-content-center row-hidden-button">
								<div class="col-auto">
									<div class="more-link load-more-posts">
										<?= lang_text(['he' => 'טען עוד..', 'en' => 'Load more'], 'he'); ?>
									</div>
								</div>
							</div>
						<?php endif; ?>
					</div>
				</div>
				<?php if ($published_posts && $published_posts > 3) : ?>
					<div class="row justify-content-center row-main-button">
						<div class="col-auto">
							<div class="more-link load-more-posts">
								<?= lang_text(['he' => 'טען עוד..', 'en' => 'Load more'], 'he'); ?>
							</div>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</article>
<?php
get_template_part('views/partials/repeat', 'form');
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider',
		[
			'content' => $fields['single_slider_seo'],
			'img' => $fields['slider_img'],
		]);
}
if ($fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
		[
			'title' => $fields['faq_title'],
			'faq' => $fields['faq_item'],
		]);
endif;
get_footer(); ?>
