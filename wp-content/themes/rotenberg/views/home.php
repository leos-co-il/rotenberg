<?php
/*
Template Name: דף הבית
*/

get_header();
$fields = get_fields();

?>

<?php if ($main_slider = $fields['main_slider']) : ?>
	<section class="main-block">
		<div class="main-slider" dir="rtl">
			<?php foreach ($main_slider as $slide) : ?>
				<div class="main-slide-item"
						<?php if ($slide_img = $slide['main_img']) : ?>
							style="background-image: url('<?= $slide_img['url']; ?>')"
						<?php endif; ?>>
					<?php if ($slide['main_text']) : ?>
						<div class="slide-main-wrap">
							<div class="base-output">
								<?= $slide['main_text']; ?>
							</div>
						</div>
					<?php endif; ?>
				</div>
			<?php endforeach; ?>
		</div>
	</section>
<?php endif; ?>
<section class="blue-form">
	<?php get_template_part('views/partials/repeat', 'form', [
			'title' => $fields['h_form_title'],
			'id' => '12',
	]); ?>
</section>
<?php if ($fields['h_about_text'] || $fields['h_about_img'] || $fields['h_achievements']) : ?>
	<section class="home-about-block">
		<div class="container">
			<div class="row justify-content-between align-items-center mb-5">
				<?php if($fields['h_about_img']) : ?>
					<div class="col-xl-4 col-lg-5 col-about-img">
						<div class="about-img-wrap">
							<img src="<?= $fields['h_about_img']['url']; ?>" alt="about-img" class="w-100">
						</div>
					</div>
				<?php endif; ?>
				<div class="col-lg-6 col-12 d-flex flex-column align-items-start">
					<div class="base-output mb-3">
						<?= $fields['h_about_text']; ?>
					</div>
					<?php if ($fields['h_about_link']) : ?>
						<a href="<?= $fields['h_about_link']['url'];?>" class="base-link">
							<?= (isset($fields['h_about_link']['title']) && $fields['h_about_link']['title'])
									? $fields['h_about_link']['title'] : lang_text(['he' => 'קרא עוד עלינו', 'en' => 'Read more about us'], 'he');
							?>
						</a>
					<?php endif; ?>
				</div>
			</div>
		</div>
		<?php if ($fields['h_achievements']) {
			get_template_part('views/partials/content', 'achievements',
					[
							'item' => $fields['h_achievements'],
					]);
		}
		if ($fields['h_areas_item']) {
			get_template_part('views/partials/content', 'areas',
					[
							'title' => $fields['h_areas_title'],
							'item' => $fields['h_areas_item'],
					]);
		} ?>
	</section>
<?php endif; ?>
<?php if ($fields['h_why_gallery'] || $fields['h_why_item']) {
	get_template_part('views/partials/content', 'benefits',
			[
					'title' => $fields['h_why_title'],
					'img' => $fields['h_why_gallery'],
					'items' => $fields['h_why_item'],
			]);
}
get_template_part('views/partials/repeat', 'form');
if ($fields['home_tabs']) : ?>
	<section class="home-posts-block">
		<div class="container">
			<?php if ($fields['h_blog_title']) : ?>
				<div class="row justify-content-center">
					<div class="col">
						<h2 class="base-title text-center mb-4">
							<?= $fields['h_blog_title']; ?>
						</h2>
					</div>
				</div>
			<?php endif; ?>
			<div class="row">
				<div class="col-12">
					<ul class="nav nav-tabs" id="myTab" role="tablist">
						<?php foreach ($fields['home_tabs'] as $x => $tab) : if ($tab['tab_title']) : ?>
							<li class="nav-item col-auto link-item-col">
								<a class="nav-link <?= ($x === 0) ? 'active' : ''; ?>" id="<?= $x; ?>-tab"
								   data-toggle="tab" href="#content-<?= $x; ?>" role="tab" aria-controls="content-<?= $x; ?>"
								   aria-selected="false">
									<?= $tab['tab_title']; ?>
								</a>
							</li>
						<?php endif; endforeach; ?>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<div class="tab-content mt-0" id="myTabContent">
						<?php foreach ($fields['home_tabs'] as $x => $tab) : if ($tab['tab_posts']) : ?>
							<div class="tab-pane fade <?= ($x === 0) ? 'show active' : ''; ?>"
								 id="content-<?= $x; ?>" role="tabpanel" aria-labelledby="<?= $x; ?>-tab">
								<div class="row">
									<?php foreach ($tab['tab_posts'] as $i => $post) {
										get_template_part('views/partials/card', 'post', [
												'post' => $post,
										]);
									}  ?>
								</div>
							</div>
						<?php endif; endforeach; ?>
					</div>
				</div>
			</div>
			<?php if ($fields['h_blog_link']) : ?>
				<div class="row justify-content-end">
					<div class="col-auto">
						<a href="<?= $fields['h_blog_link']['url'];?>" class="base-link">
							<?= (isset($fields['h_blog_link']['title']) && $fields['h_blog_link']['title'])
									? $fields['h_blog_link']['title'] : lang_text(['he' => 'לכל המאמרים', 'en' => 'To all articles'], 'he');
							?>
						</a>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</section>
<?php endif;
if ($fields['h_slider_seo']) {
	get_template_part('views/partials/content', 'slider',
			[
					'content' => $fields['h_slider_seo'],
					'img' => $fields['h_slider_img'],
			]);
} ?>
<?php if ($fields['h_reviews']) : ?>
	<section class="reviews-block">
		<div class="container">
			<?php if ($fields['h_reviews_title']) : ?>
				<div class="row justify-content-center">
					<div class="col-auto">
						<h2 class="base-title text-center mb-4"><?= $fields['h_reviews_title']; ?></h2>
					</div>
				</div>
			<?php endif; ?>
			<div class="row justify-content-center arrows-slider arrows-slider-reviews">
				<div class="col-12">
					<div class="reviews-slider" dir="rtl">
						<?php foreach ($fields['h_reviews'] as $x => $review) : ?>
							<div class="review-slide">
								<div class="review-item">
									<img src="<?= ICONS ?>quote-top.png" alt="quotes" class="quote quote-top">
									<img src="<?= ICONS ?>quote-bottom.png" alt="quotes" class="quote quote-bottom">
									<div class="rev-pop-trigger">
										+
										<div class="hidden-review">
											<h3 class="base-item-title review-title"><?= $review['review_name']; ?></h3>
											<div class="base-output">
												<?= $review['review_text']; ?>
											</div>
										</div>
									</div>
									<div class="rev-content">
										<div class="d-flex justify-content-start align-items-center mb-3">
											<h3 class="base-item-title review-title"><?= $review['review_name']; ?></h3>
											<?php if ($review['review_logo']) : ?>
												<img src="<?= $review['review_logo']['url']; ?>" alt="logo-review">
											<?php endif; ?>
										</div>
										<div class="base-text">
											<?= text_preview($review['review_text'], '30'); ?>
										</div>
									</div>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<div class="modal fade" id="reviewsModal" tabindex="-1" role="dialog"
		 aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
			<div class="modal-content">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<i class="fas fa-times"></i>
				</button>
				<div class="modal-body" id="reviews-pop-wrapper"></div>
			</div>
		</div>
	</div>
<?php endif;
if ($fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
			[
					'title' => $fields['faq_title'],
					'faq' => $fields['faq_item'],
			]);
endif;
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider',
			[
					'content' => $fields['single_slider_seo'],
					'img' => $fields['slider_img'],
			]);
}
get_footer(); ?>
