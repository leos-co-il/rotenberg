<?php
/*
Template Name: צור קשר
*/

get_header();
$fields = get_fields();

$tel = opt('tel');
$tel_2 = opt('tel_2');
$mail = opt('mail');
$mail_2 = opt('mail_2');
$address = opt('address');
$fax = opt('fax');
$map = opt('map_image');
?>

<article class="page-body p-100">
	<?php if ( function_exists('yoast_breadcrumb')) : ?>
		<div class="container-fluid pt-3 mb-4">
			<div class="row justify-content-center">
				<div class="col-xl-11 col-12">
					<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' ); ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<div class="contact-page-body pt-3">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-xl-10 col-lg-11 col-12">
					<div class="base-output text-center mb-5">
						<h2><?php the_title(); ?></h2>
						<?php the_content(); ?>
					</div>
				</div>
				<div class="col-xl-10 col-lg-11 col-12">
					<div class="row justify-content-center">
						<?php if ($tel) : ?>
							<div class="col-lg-3 col-sm-6 col-11 contact-item contact-item-link wow zoomIn"
								 data-wow-delay="0.2s">
								<div class="contact-icon-wrap">
									<img src="<?= ICONS ?>contact-tel.png">
								</div>
								<a href="tel:<?= $tel; ?>" class="contact-info">
									<?= $tel; ?>
								</a>
							</div>
						<?php endif;
						if ($fax) : ?>
							<div class="contact-item col-lg-3 col-sm-6 col-11 wow zoomIn" data-wow-delay="0.4s">
								<div class="contact-icon-wrap">
									<img src="<?= ICONS ?>contact-fax.png">
								</div>
								<div class="contact-info">
									<?= $fax; ?>
								</div>
							</div>
						<?php endif;
						if ($mail) : ?>
							<div class="contact-item col-lg-3 col-sm-6 col-11 contact-item-link wow zoomIn"
								 data-wow-delay="0.6s">
								<div class="contact-icon-wrap">
									<img src="<?= ICONS ?>contact-mail.png">
								</div>
								<a href="mailto:<?= $mail; ?>" class="contact-info">
									<?= $mail; ?>
								</a>
							</div>
						<?php endif; ?>
						<?php if ($address) : ?>
							<div class="contact-item-link contact-item col-lg-3 col-sm-6 col-11 wow zoomIn" data-wow-delay="0.8s">
								<div class="contact-icon-wrap">
									<img src="<?= ICONS ?>contact-geo.png">
								</div>
								<a class="contact-info" href="https://www.waze.com/ul?q=<?= $address; ?>">
									<?= $address; ?>
								</a>
							</div>
						<?php endif; ?>
					</div>
				</div>
				<div class="col-xl-10 col-lg-11 col-12">
					<div class="contact-form-back">
						<?php if ($fields['contact_form_title'] || $fields['contact_form_subtitle']) : ?>
							<div class="row justify-content-center align-items-center mb-3">
								<div class="col-auto">
									<h2 class="contact-form-title">
										<?= $fields['contact_form_title']; ?>
									</h2>
								</div>
								<div class="col-auto">
									<h2 class="contact-form-subtitle font-weight-normal">
										<?= $fields['contact_form_subtitle']; ?>
									</h2>
								</div>
							</div>
						<?php endif;
						getForm('15'); ?>
					</div>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-xl-10 col-lg-11">
					<?php if ($map) : ?>
						<a class="map-image" href="<?= $map['url']; ?>" data-lightbox="map">
							<img src="<?= $map['url']; ?>" alt="map">
						</a>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</article>
<?php get_footer(); ?>
