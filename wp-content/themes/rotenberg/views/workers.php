<?php
/*
Template Name: צוות המשרד
*/

get_header();
$fields = get_fields();
$posts = new WP_Query([
	'posts_per_page' => -1,
	'post_type' => 'worker',
	'suppress_filters' => false,
]);
?>

<article class="article-page-body page-body">
	<?php if ( function_exists('yoast_breadcrumb')) : ?>
		<div class="container-fluid pt-3 mb-4">
			<div class="row justify-content-center">
				<div class="col-xl-11 col-12">
					<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' ); ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<div class="container pt-3">
		<div class="row justify-content-center">
			<div class="col-md-10 col-12">
				<h1 class="base-title text-center mb-3"><?php the_title(); ?></h1>
				<div class="base-output text-center">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</div>
	<?php if ($posts->have_posts()) : ?>
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-xl-11 col-12">
					<div class="row justify-content-center align-items-stretch put-here-posts">
						<?php foreach ($posts->posts as $i => $post) {
							get_template_part('views/partials/card', 'worker', [
								'post' => $post,
							]);
						} ?>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
</article>
<?php get_footer(); ?>
