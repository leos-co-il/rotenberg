<?php
/*
Template Name: אודות
*/

get_header();
$fields = get_fields();


?>

<article class="page-body pb-0">
	<?php if ( function_exists('yoast_breadcrumb')) : ?>
		<div class="container-fluid pt-3">
			<div class="row justify-content-center">
				<div class="col-xl-11 col-12">
					<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' ); ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<div class="container pt-3">
		<div class="row justify-content-between align-items-center mb-5">
			<?php if(has_post_thumbnail()) : ?>
				<div class="col-xl-4 col-lg-5 col-about-img">
					<div class="about-img-wrap">
						<img src="<?= postThumb(); ?>" alt="about-img" class="w-100">
					</div>
				</div>
			<?php endif; ?>
			<div class="col-lg-6 col-12 d-flex flex-column align-items-start">
				<div class="base-output bigger-output mb-3">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</div>
	<?php if ($fields['about_achievements']) {
			get_template_part('views/partials/content', 'achievements',
					[
							'item' => $fields['about_achievements'],
					]);
		} ?>
</article>
<?php
if ($fields['about_why_gallery'] || $fields['about_why_item']) {
	get_template_part('views/partials/content', 'benefits',
			[
					'title' => $fields['about_why_title'],
					'img' => $fields['about_why_gallery'],
					'items' => $fields['about_why_item'],
			]);
}
get_template_part('views/partials/repeat', 'form');
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider',
		[
			'content' => $fields['single_slider_seo'],
			'img' => $fields['slider_img'],
		]);
}
if ($fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
		[
			'title' => $fields['faq_title'],
			'faq' => $fields['faq_item'],
		]);
endif;
get_footer(); ?>
