<?php if (isset($args['post']) && $args['post']) : $link = get_the_permalink($args['post']); ?>
	<div class="col-12 col-post">
		<div class="post-card more-card" data-id="<?= $args['post']->ID; ?>">
			<a class="post-img"<?php if (has_post_thumbnail($args['post'])) : ?>
				style="background-image: url('<?= postThumb($args['post']); ?>')" <?php endif; ?> href="<?= $link; ?>">
			</a>
			<div class="post-card-content">
				<a class="base-item-title" href="<?= $link; ?>"><?= $args['post']->post_title; ?></a>
				<p class="base-text">
					<?= text_preview($args['post']->post_content, 50); ?>
				</p>
				<a href="<?= $link; ?>" class="base-link mt-3">
					<?= lang_text(['he' => 'המשך קריאה', 'en' => 'Continue reading'], 'he'); ?>
				</a>
			</div>
		</div>
	</div>
<?php endif; ?>
