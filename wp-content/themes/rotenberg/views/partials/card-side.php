<?php $item = (isset($args['item']) && $args['item']) ? $args['item'] : opt('basic_side_item');
if ($item) : ?>
	<div class="blue-item-wrap">
		<?php if ($args['item']['side_img']) : ?>
			<div class="side-img-wrap">
				<img src="<?= $args['item']['side_img']['url']; ?>" alt="image">
			</div>
		<?php endif; ?>
		<h3 class="side-item-title align-self-start">
			<?= $args['item']['side_title']; ?>
		</h3>
		<h3 class="side-item-text">
			<?= $args['item']['side_text']; ?>
		</h3>
	</div>
<?php endif; ?>
