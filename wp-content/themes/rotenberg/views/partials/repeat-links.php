<?php $links= (isset($args['links']) && $args['links']) ? $args['links'] : opt('basic_links');
if ($links) : ?>
	<div class="row justify-content-center align-items-stretch">
		<?php foreach ($links as $link) : if ($link['link_url']) : ?>
			<div class="col-auto link-item-col">
				<a href="<?= $link['link_url']['url']; ?>">
					<?= $link['link_title'] ? $link['link_title'] : $link['link_url']['title']; ?>
				</a>
			</div>
		<?php endif; endforeach; ?>
	</div>
<?php endif; ?>
