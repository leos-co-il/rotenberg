<?php if( isset($args['item']) && $args['item']) : ?>
	<div class="achivs-block">
		<div class="container">
			<div class="row justify-content-center align-items-start">
				<?php foreach ($args['item'] as $item) : ?>
					<div class="col-lg-3 col-sm-6 col-12 area-item-col">
						<h4 class="base-item-title"><?= $item['ach_title']; ?></h4>
						<p class="base-text"><?= $item['ach_text']; ?></p>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
<?php endif; ?>
