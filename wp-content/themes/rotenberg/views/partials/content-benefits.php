<?php if (((isset($args['items']) && $args['items']) || (isset($args['img'])) && $args['img'])) : ?>
	<div class="why-us-block">
		<div class="container">
			<?php if ((isset($args['title'])) && $args['title']) : ?>
				<div class="row justify-content-start">
					<div class="col-auto">
						<h2 class="base-title"><?= $args['title']; ?></h2>
					</div>
				</div>
			<?php endif; ?>
			<div class="row justify-content-center align-items-stretch">
				<div class="<?= ((isset($args['img'])) && $args['img']) ? 'col-xl-6 col-12' : 'col-12'; ?> mb-xl-0 mb-5">
					<?php if ((isset($args['items']) && $args['items'])) : ?>
						<div class="row row-why-us align-items-start justify-content-lg-start justify-content-center h-100">
						<?php foreach ($args['items'] as $num => $why_item) : ?>
							<div class="col-xl-11 col-12 why-item wow fadeInUp"
								 data-wow-delay="0.<?= $num; ?>s">
								<div class="row align-items-start">
									<div class="col-sm-auto mb-sm-0 mb-3 number-circle-col">
										<div class="why-icon-wrap">
											<?php if ($why_item_icon = $why_item['why_icon']) : ?>
												<img src="<?= $why_item_icon['url']; ?>">
											<?php endif; ?>
										</div>
									</div>
									<div class="col">
										<div class="why-content-wrap">
											<h3 class="why-item-title">
												<?= $why_item['why_title']; ?>
											</h3>
											<div class="base-text">
												<?= $why_item['why_text']; ?>
											</div>
										</div>
									</div>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
					<?php endif; ?>
				</div>
				<?php if (((isset($args['img'])) && $args['img'])) : ?>
					<div class="col-xl-6 col-12">
						<div class="row justify-content-center align-items-stretch">
							<?php foreach ($args['img'] as $y => $img) : ?>
								<div class="col-sm-6 col-12 col-why-img-item wow zoomIn" data-wow-delay="0.<?= $y; ?>s">
									<div class="why-image" style="background-image: url('<?= $img['url']; ?>')"></div>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
<?php endif; ?>
