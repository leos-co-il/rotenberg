<?php if( isset($args['item']) && $args['item']) : ?>
	<div class="areas-block">
		<div class="container">
			<?php if( isset($args['title']) && $args['title']) : ?>
				<div class="row justify-content-center">
					<div class="col-auto">
						<h2 class="base-title mb-4"><?= $args['title']; ?></h2>
					</div>
				</div>
			<?php endif; ?>
			<div class="row justify-content-center align-items-start">
				<?php foreach ($args['item'] as $item) : ?>
					<div class="col-lg-3 col-sm-6 col-8 area-item-col">
						<a class="area-item" href="<?= isset($item['area_link']) && $item['area_link'] ? $item['area_link']['url'] : ''; ?>">
							<div class="area-icon-wrap">
								<?php if ($item['area_icon']) : ?>
									<img src="<?= $item['area_icon']['url']; ?>" alt="area-of-expertise">
								<?php endif; ?>
							</div>
							<span class="area-title"><?= $item['area_title']; ?></span>
						</a>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
<?php endif; ?>
