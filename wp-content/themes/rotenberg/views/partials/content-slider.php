<?php if (isset($args['content']) && ($args['content'])) :
	$slider_img = isset($args['img']) ? ($args['img']) : ''; ?>
	<div class="base-slider-block arrows-slider">
		<div class="container">
			<div class="row">
				<?php if ($slider_img) : ?>
					<div class="col-12 slider-img-col">
						<img src="<?= $slider_img['url']; ?>" class="img-slider">
					</div>
				<?php endif; ?>
				<div class="col-12">
					<div class="base-slider-wrap">
						<div class="base-slider" dir="rtl">
							<?php foreach ($args['content'] as $content) : ?>
								<div class="slider-base-item">
									<div class="base-output slider-output">
										<?= $content['content']; ?>
									</div>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
