<?php $f_title = (isset($args['title']) && $args['title']) ? $args['title'] : opt('base_form_title');
$id = (isset($args['id']) && $args['id']) ? $args['id'] : '13';
?>
<div class="repeat-form-block">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-11 col-12">
				<?php if ($f_title) : ?>
					<h2 class="form-title"><?= $f_title; ?></h2>
				<?php endif; ?>
				<div class="base-form-wrap">
					<?php getForm($id); ?>
				</div>
			</div>
		</div>
	</div>
</div>
