<?php if (isset($args['post']) && $args['post']) : $link = get_the_permalink($args['post']); ?>
	<div class="col-xl-4 col-md-6 col-sm-10 col-12 col-worker">
		<div class="worker-card more-card" data-id="<?= $args['post']->ID; ?>">
			<a href="<?= $link; ?>" class="worker-img-wrap">
				<div class="worker-img"<?php if (has_post_thumbnail($args['post'])) : ?>
					style="background-image: url('<?= postThumb($args['post']); ?>')" <?php endif; ?>>
				</div>
			</a>
			<a href="<?= $link; ?>" class="base-mid-title">
				<?= $args['post']->post_title; ?>
			</a>
			<h3 class="worker-position-title">
				<?= get_field('worker_position', $args['post']->ID); ?>
			</h3>
		</div>
	</div>
<?php endif; ?>
