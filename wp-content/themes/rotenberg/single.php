<?php
the_post();
get_header();
$fields = get_fields();
$post_link = get_the_permalink();
?>

<article class="article-page-body page-body">
	<?php if ( function_exists('yoast_breadcrumb')) : ?>
		<div class="container-fluid pt-3">
			<div class="row justify-content-center">
				<div class="col-xl-11 col-12">
					<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' ); ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-lg-11 col-12">
				<h1 class="base-title text-center mb-3"><?php the_title(); ?></h1>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-lg-11 col-12">
				<div class="row justify-content-center align-items-start">
					<div class="col-xl-3 col-12 col-sidebar">
						<?php if ($fields['post_side_block']) {
							get_template_part('views/partials/card', 'side', [
								'item' => $fields['post_side_block'],
							]);
						}
						get_template_part('views/partials/repeat', 'form_side');
						if ($fields['post_side_links']) : foreach ($fields['post_side_links'] as $link_block) : ?>
							<div class="blue-item-wrap align-items-stretch">
								<?php if ($link_block) : ?>
									<h3 class="links-block-title"><?= $link_block['links_block_title']; ?></h3>
								<?php endif;
								if ($link_block['links_items']) : foreach ($link_block['links_items'] as $link) :
									if (isset($link['side_link']['title']) && $link['side_link']['title']) : ?>
									<a href="<?= $link['side_link']['url']; ?>" class="links-block-item">
										<?= $link['side_link']['title']; ?>
									</a>
								<?php endif; endforeach; endif; ?>
							</div>
						<?php endforeach; endif; ?>
					</div>
					<div class="col-xl-9 col-12 col-content-side mb-5">
						<div class="base-output bigger-output post-body-out">
							<?php the_content(); ?>
						</div>
						<div class="socials-share">
							<span class="share-text">
								<?= lang_text(['he' => 'שתף', 'en' => 'Share'], 'he'); ?>
							</span>
							<!--	WHATSAPP-->
							<a href="https://api.whatsapp.com/send?text=<?php the_title(); echo $post_link; ?>" class="social-share-link">
								<img src="<?= ICONS ?>whatsapp-share.png">
							</a>
							<a href="https://www.facebook.com/sharer/sharer.php?u=#<?= $post_link; ?>" target="_blank"
							   class="social-share-link">
								<img src="<?= ICONS ?>facebook-share.png">
							</a>
							<!--	MAIL-->
							<a href="mailto:?subject=&body=<?= $post_link; ?>" target="_blank"
							   class="social-share-link">
								<img src="<?= ICONS ?>mail-share.png">
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</article>
<?php
$postId = get_the_ID();
$post_terms = wp_get_object_terms($postId, 'category', ['fields' => 'ids']);
$samePosts = [];
$samePosts = get_posts([
		'posts_per_page' => 2,
		'post_type' => 'post',
		'post__not_in' => array($postId),
		'tax_query' => [
				[
						'taxonomy' => 'category',
						'field' => 'term_id',
						'terms' => $post_terms,
				],
		],
]);
if ($fields['same_posts']) {
	$samePosts = $fields['same_posts'];
} elseif ($samePosts === NULL) {
	$samePosts = get_posts([
			'posts_per_page' => 2,
			'orderby' => 'rand',
			'post_type' => 'post',
			'post__not_in' => array($postId),
	]);
}
if ($samePosts) : ?>
	<section class="same-posts mt-5">
		<div class="container">
			<div class="row justify-content-center align-items-stretch">
				<div class="col-12">
					<h2 class="base-title text-center mb-4">
						<?= $fields['same_title'] ? $fields['same_title'] :
								lang_text(['he' => 'למאמרים נוספים באותו תחום', 'en' => 'More articles from the same field'], 'he'); ?>
					</h2>
				</div>
				<?php foreach ($samePosts as $i => $post) {
					get_template_part('views/partials/card', 'post', [
							'post' => $post,
					]);
				} ?>
			</div>
		</div>
	</section>
<?php endif;
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider',
		[
			'content' => $fields['single_slider_seo'],
			'img' => $fields['slider_img'],
		]);
}
if ($fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
		[
			'title' => $fields['faq_title'],
			'faq' => $fields['faq_item'],
		]);
endif;
get_footer(); ?>
