(function($) {
	$.fn.slideFadeToggle  = function(speed, easing, callback) {
		return this.animate({opacity: 'toggle', height: 'toggle'}, speed, easing, callback);
	};
	var linc2 = $('.drop-container'),
		timeoutId;
	$('.first-sub-menu').hover(function(){
		var content = $(this).children('.sub-menu').clone();
		if(content) {
			linc2.html(content);
			clearTimeout(timeoutId);
			linc2.show();
		}
	}, function(){
		timeoutId = setTimeout($.proxy(linc2,'hide'), 1000);
	});
	linc2.mouseenter(function(){
		clearTimeout(timeoutId);
	}).mouseleave(function(){
		linc2.hide();
	});
	$( document ).ready(function() {
		var menu = $( '.drop-menu' );
		$('.hamburger').click(function () {
			menu.slideFadeToggle();
			$(this).toggleClass('is-active');
		});
		$( '.rev-pop-trigger' ).click( function() {
			var contentAll = $( this ).children('.hidden-review').html();
			$( '#reviews-pop-wrapper' ).html( contentAll );
			$( '#reviewsModal' ).modal( 'show' );
		});
		$( '#reviewsModal' ).on( 'hidden.bs.modal', function( e ) {
			$( '#reviews-pop-wrapper' ).html( '' );
		});
		$('.search-trigger').click(function () {
			$('.search-header').addClass('show-search');
		});
		$('.close-search').click(function () {
			$('.search-header').removeClass('show-search');
		});
		$('.pop-trigger').click(function () {
			$('.float-form').toggleClass('show-form');
		});
		$('.base-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			rtl: true,
			arrows: true,
			dots: false,
		});
		$('.news-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			autoplay: true,
			autoplaySpeed: 2000,
			rtl: true,
			arrows: false,
			dots: false,
		});
		$('.main-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			rtl: true,
			arrows: false,
			dots: true,
		});
		$('.reviews-slider').slick({
			slidesToShow: 2,
			slidesToScroll: 1,
			rtl: true,
			arrows: true,
			dots: false,
			responsive: [
				{
					breakpoint: 992,
					settings: {
						slidesToShow: 1,
					}
				},
			]
		});
		$( function() {
			$.fn.scrollToTop = function() {
				$( this ).click( function() {
					$( 'html, body' ).animate({scrollTop: 0}, 'slow' );
				});
			};
		});
		$( function() {
			$( '#go-top' ).scrollToTop();
		});
		$('.play-button').click(function() {
			var id = $(this).data('video');
			var frame = '<iframe width="100%" height="500px" src="https://www.youtube.com/embed/'+id+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
			$('#iframe-wrapper').html(frame);
			$('#modalCenter').modal('show');
		});
		$('#modalCenter').on('hidden.bs.modal', function (e) {
			$('#iframe-wrapper').html('');
		});
		var accordion = $('#accordion');
		accordion.on('shown.bs.collapse', function () {
			var show = $( '.show' );
			show.parent().children('.question-title').addClass('active-faq');
		});
		accordion.on('hidden.bs.collapse', function () {
			var collapsed = $( '.collapse' );
			collapsed.parent().children('.question-title').removeClass('active-faq');
		});
		//Socials
		$('.trigger-wrap').hover(function(){
			$(this).children('.all-socials').addClass('show-share');
		}, function(){
			$(this).children('.all-socials').removeClass('show-share');
		});
	});

	$('.load-more-posts').click(function() {
		var termID = $(this).data('term');
		// var params = $('.take-json').html();
		var ids = '';
		var video = $(this).data('content');
		var page = $(this).data('page');

		$('.more-card').each(function(i, obj) {
			ids += $(obj).data('id') + ',';
		});
		jQuery.ajax({
			url: '/wp-admin/admin-ajax.php',
			dataType: 'json',
			data: {
				// postType: postType,
				termID: termID,
				ids: ids,
				page: page,
				video: video,
				// taxType: taxType,
				action: 'get_more_function',
			},
			success: function (data) {
				if (!data.html) {
					$('.load-more-posts').addClass('hide');
				}
				$('.put-here-posts').append(data.html);
			}
		});
	});
})( jQuery );
