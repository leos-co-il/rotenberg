<!DOCTYPE HTML>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <link rel="icon" href="<?= IMG ?>favicon.ico" type="image/x-icon">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <?php wp_head(); ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body <?php body_class(ENV); ?>>

<?php if(ENV === 'dev'): ?>
    <script>
        const timerStart = Date.now();
    </script>
<div class="debug bg-danger border">
    <p class="width">
        <span>Width:</span>
        <span class="val"></span>
    </p>
    <p class="height">
        <span>Height:</span>
        <span class="val"></span>
    </p>
    <p class="media-query">
        <span>Media Query:</span>
        <span class="val"></span>
    </p>
    <p class="zoom">
        <span>Zoom:</span>
        <span class="val"></span>
    </p>
    <p class="dom-ready">
        <span>DOM Ready:</span>
        <span class="val"></span>
    </p>
    <p class="load-time">
        <span>Loading Time:</span>
        <span class="val"></span>
    </p>
</div>
<?php endif; ?>


<header>
	<?php if ($flash_msg = opt('news')) : ?>
		<div class="header-news">
			<div class="container-fluid">
				<div class="row justify-content-center">
					<div class="col">
						<div class="news-slider" dir="rtl">
							<?php foreach ($flash_msg as $msg): ?>
								<div class="single-msg d-flex centered">
									<?php if($msg['news_link']): ?>
									<a href="<?= (isset($msg['news_link']['url']) && $msg['news_link']['url']) ? $msg['news_link']['url'] : ''; ?>"
									   title="<?= $msg['news_title'] ?>" class="msg-link">
										<span class="msg-text"><?= $msg['news_title'] ?></span>
									</a>
									<?php endif; ?>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<div class="header-top">
		<div class="container-fluid">
			<div class="row justify-content-between align-items-end">
				<div class="col d-flex justify-content-start align-items-center">
					<?php if ($imgs = opt('header_imgs')) : ?>
						<div class="images-header">
							<?php foreach ($imgs as $img) : ?>
								<img src="<?= $img['url']; ?>" alt="sign">
							<?php endforeach; ?>
						</div>
					<?php endif; ?>
				</div>
				<?php if ($logo = opt('logo')) : ?>
					<div class="col-md-auto col-sm-3 col-4 d-flex justify-content-center align-items-center">
						<a href="/" class="logo">
							<img src="<?= $logo['url'] ?>" alt="logo">
						</a>
					</div>
				<?php endif; ?>
				<div class="col d-flex justify-content-end align-items-center p-col-0">
					<div class="search-trigger">
						<img src="<?= ICONS ?>search.png" alt="search">
					</div>
					<div class="search-header">
						<span class="close-search">x</span>
						<?php get_search_form(); ?>
					</div>
					<div class="langs-wrap">
						<?php site_languages(); ?>
					</div>
					<?php if ($tel = opt('tel')) : ?>
						<a href="tel:<?= $tel; ?>" class="header-tel">
							<img src="<?= ICONS ?>header-tel.png" alt="phone">
							<span class="tel-number"><?= $tel; ?></span>
						</a>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
	<div class="header-bottom">
		<div class="drop-container">
		</div>
		<div class="container-fluid">
			<div class="row justify-content-between align-items-center position-relative">
				<div class="drop-menu">
					<nav class="drop-nav">
						<?php getMenu('dropdown-menu', '1'); ?>
					</nav>
				</div>
				<div class="col">
					<button class="hamburger hamburger--spin menu-trigger" type="button">
					<span class="hamburger-box">
						<span class="hamburger-inner"></span>
					</span>
					</button>
				</div>
				<div class="col-auto">
					<nav id="desktopNav">
						<?php getMenu('header-menu', '2', '', ''); ?>
					</nav>
				</div>
				<div class="col"></div>
			</div>
		</div>
	</div>
</header>

<?php
$tel = opt('tel');
$whatsapp = opt('whatsapp');
$facebook = opt('facebook');
$instagram = opt('instagram');
$youtube = opt('youtube');
?>
<div class="socials-fix">
	<?php if ($whatsapp) : ?>
		<a href="https://api.whatsapp.com/send?phone=<?= $whatsapp; ?>" target="_blank" class="social-link whatsapp-l">
			<i class="fab fa-whatsapp"></i>
		</a>
	<?php endif; ?>
	<?php if ($facebook) : ?>
		<a href="<?= $facebook; ?>" target="_blank" class="social-link facebook-l">
			<i class="fab fa-facebook-f"></i>
		</a>
	<?php endif; ?>
	<?php if ($instagram) : ?>
		<a href="<?= $instagram; ?>" target="_blank" class="social-link instagram-l">
			<i class="fab fa-instagram"></i>
		</a>
	<?php endif; ?>
	<?php if ($youtube) : ?>
		<a href="<?= $youtube; ?>" target="_blank" class="social-link youtube-l">
			<img src="<?= ICONS ?>youtube.png">
		</a>
	<?php endif; ?>
	<div class="pop-trigger social-link">
		<img src="<?= ICONS ?>pop-trigger.png">
	</div>
</div>
<div class="float-form">
	<div class="container-fluid">
		<div class="row justify-content-end align-items-end">
			<div class="col-lg-6 col-md-8 col-sm-9 col-12 col-xxl">
				<div class="pop-form">
					<div class="sidebar-form blue-item-wrap">
						<h3 class="side-title"><?= opt('pop_form_title'); ?></h3>
						<h4 class="side-subtitle"><?= opt('pop_form_subtitle'); ?></h4>
						<h5 class="side-text"><?= opt('pop_form_subtitle_small'); ?></h5>
						<?php getForm('11'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
