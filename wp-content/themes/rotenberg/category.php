<?php
get_header();
$query = get_queried_object();
$posts = new WP_Query([
		'posts_per_page' => 3,
		'post_type' => 'post',
		'suppress_filters' => false,
		'tax_query' => [
				[
						'taxonomy' => 'category',
						'field' => 'term_id',
						'terms' => $query->term_id,
				]
		]
]);
$published_posts = get_posts([
		'numberposts' => -1,
		'post_type' => 'post',
		'suppress_filters' => false,
		'tax_query' => [
				[
						'taxonomy' => 'category',
						'field' => 'term_id',
						'terms' => $query->term_id,
				]
		]
]);
?>
<article class="article-page-body page-body">
	<?php if ( function_exists('yoast_breadcrumb')) : ?>
		<div class="container-fluid pt-3">
			<div class="row justify-content-center">
				<div class="col-xl-11 col-12">
					<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' ); ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-lg-11 col-12">
				<h1 class="base-title text-center mb-3"><?= $query->name; ?></h1>
				<?php if ($links = get_field('blog_links', $query)) {
					get_template_part('views/partials/repeat', 'links', [
						'links' => $links,
					]);
				} ?>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-lg-11 col-12">
				<div class="row justify-content-center align-items-start">
					<div class="col-xl-3 col-12 col-sidebar">
						<?php if ($blog_item = get_field('blog_side_item', $query)) {
							get_template_part('views/partials/card', 'side', [
								'item' => $blog_item,
							]);
						}
						get_template_part('views/partials/repeat', 'form_side'); ?>
					</div>
					<div class="col-xl-9 col-12 col-content-side">
						<div class="base-output bigger-output mb-5">
							<?= category_description(); ?>
						</div>
						<?php if ($posts->have_posts()) : ?>
							<div class="row justify-content-center align-items-stretch put-here-posts">
								<?php foreach ($posts->posts as $i => $post) {
									get_template_part('views/partials/card', 'post', [
										'post' => $post,
									]);
								}  ?>
							</div>
						<?php endif;
						if ($published_posts && $published_posts > 3) : ?>
							<div class="row justify-content-center row-hidden-button">
								<div class="col-auto">
									<div class="more-link load-more-posts">
										<?= lang_text(['he' => 'טען עוד..', 'en' => 'Load more'], 'he'); ?>
									</div>
								</div>
							</div>
						<?php endif; ?>
					</div>
				</div>
				<?php if ($published_posts && $published_posts > 3) : ?>
					<div class="row justify-content-center row-main-button">
						<div class="col-auto">
							<div class="more-link load-more-posts" data-term="<?= $query->term_id; ?>">
								<?= lang_text(['he' => 'טען עוד..', 'en' => 'Load more'], 'he'); ?>
							</div>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</article>
<?php
get_template_part('views/partials/repeat', 'form');
if ($seo = get_field('single_slider_seo', $query)) {
	get_template_part('views/partials/content', 'slider',
		[
			'content' => $seo,
			'img' => get_field('slider_img', $query),
		]);
}
if ($faq = get_field('faq_item', $query)) :
	get_template_part('views/partials/content', 'faq',
		[
			'title' => get_field('faq_title', $query),
			'faq' => $faq,
		]);
endif;
get_footer(); ?>
